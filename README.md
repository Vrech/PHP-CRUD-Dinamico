Para usar qualquer função, basta instanciar a classe e chamar a função passando os parâmetros necessários.

Primeiramente é necessário instanciar a classe passando os dados do BD para o construtor:

    $banco = new Banco("host", "nome_do_banco", "usuario", "senha");

Depois você chama a função e passa os parâmetros:

<h5>RETORNAR TODOS REGISTROS DA TABELA</h5><p/>

    $banco->query("SELECT * FROM usuarios");


<h5>INSERIR DADOS NA TABELA</h5><p/>

    $banco->insert("usuarios", array(
        "nome" => 'Giovani',
        "idade" => '19'
    ));
    
Note que é possível passar mais de um dado para a função, para isso basta utilizar um array.


<h5>ATUALIZAR DADOS NA TABELA</h5></p>

    $banco->update("usuarios", array("nome"=>"João"), array("id"=>"1"));
    
<h5>DELETAR DADOS NA TABELA</h5></p>

    $banco->delete("usuarios", array("id" => "1"));
    
[Meu portfólio](https://giovanivrech.com.br/)